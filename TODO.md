# TODOs

**As of commit:** [a60c0fe0](https://gitlab.com/glossa/meta/-/tree/a60c0fe088349bb837ff1c75696737232f356322)

## /src/glossa/weave.clj

 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave.clj#L11) Consider empty arity/empty node and what it might return.
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave.clj#L12) Since it appears that metadata applied via tagged literal is not preserved, consider whether a more primitive method for identifying already-parsed nodes is desirable, e.g., explicit support in this function for something like [::weave/raw x].
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave.clj#L39) Benchmark processing IR instructions in parallel. Could factor at boundaries of defined words.
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave.clj#L40) Consider story for whether to escape strings based on output format.

## /src/glossa/weave/api.clj

 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave/api.clj#L59) Here and elsewhere, provide both 'glossa.weave and shorter 'weave namespace support.
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave/api.clj#L60) Catalog from section 4 of https://html.spec.whatwg.org/multipage/
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave/api.clj#L61) Support first pass of https://developer.mozilla.org/en-US/docs/Web/HTML/Element
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave/api.clj#L62) Consider HTML-facing Markdown version that liberally emits HTML
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave/api.clj#L63) Consider whether this should live in the Markdown-specific project or is fine here. Having a core, shared porcelain set of keywords and default IR mappings is convenient, for now.
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave/api.clj#L79) attr of value for machine-readable
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave/api.clj#L101) Consider global header vs. running header (posts vs. books). Same for footer.
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave/api.clj#L103) This is HTML's mark. Consider mark vs. label vs. highlight
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave/api.clj#L112) Never been fond of label as a term for this. Considering 'mark'.
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave/api.clj#L136) attr of datetime for machine-readable
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave/api.clj#L200) Consider whether or how to handle collisions here. Likely unintentional and confusing to end-user.
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave/api.clj#L245) Still not convinced output-specification is best in the dispatch value of the multimethod. But want i18n front and center.
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/src/glossa/weave/api.clj#L250) Inheritance. How to handle case of folks wanting everything but overriding a small number of evaluations?

## /output/markdown-plain/src/glossa/weave/output/markdown_plain.clj

 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/output/markdown-plain/src/glossa/weave/output/markdown_plain.clj#L9) Consider ensuring what this produces conforms to
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/output/markdown-plain/src/glossa/weave/output/markdown_plain.clj#L25) The use of IR keywords in both the tree-of-maps and actual IR will cause confusion.
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/output/markdown-plain/src/glossa/weave/output/markdown_plain.clj#L26) Either these api/emit impls should be extracted for possible reuse, or the parsing step should be replacing raw, unqualified tag keywords with either (1) IR keywords or (2) a second set of keywords meant to be a stepping stone between the user-facing and the IR keywords. The latter is a cleaner separation and wouldn't introduce ambiguity in the code base, but it adds another layer of names. That additional layer, however, would make it trivial for consumers to map their preferred keywords to existing ones and be able to leverage the custom api/emit implementations in this library.
 - [TODO](https://gitlab.com/glossa/meta/-/blob/a60c0fe088349bb837ff1c75696737232f356322home/daniel/dev/glossa/weave/output/markdown-plain/src/glossa/weave/output/markdown_plain.clj#L61) Consider hierarchy and isa? instead of this.
