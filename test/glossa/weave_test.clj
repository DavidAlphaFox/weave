(ns glossa.weave-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [glossa.weave :as nut]
   [glossa.weave.api :as api]
   [glossa.weave.ir :as ir]))

(def test-env
  (dissoc nut/default-env :output))

(def weave-doc|simple
  [:glossa.weave/doc
   {:title "Simple Doc"}
   [:p
    [:a {:href "#local"} "See here"]]
   nil
   [:p "A second paragraph."]])

(deftest test-parse|simple
  (is (= {:tag :glossa.weave/doc
          :attrs {:title "Simple Doc"}
          :content [{:tag :p
                     :attrs {}
                     :content [{:tag :a
                                :attrs {:href "#local"}
                                :content ["See here"]}]}
                    {:tag :p
                     :attrs {}
                     :content ["A second paragraph."]}]}
         (nut/parse weave-doc|simple))))

(deftest test-emit|simple
  (is (= ["See here"
          {}
          ::ir/string
          {:url "#local"}
          ::ir/link
          {}
          ::ir/paragraph
          "A second paragraph."
          {}
          ::ir/string
          {}
          ::ir/paragraph
          {:title "Simple Doc"}
          ::ir/doc]
         (->> weave-doc|simple
              (nut/parse)
              (nut/emit test-env)
              :ir))))

(deftest test-weave-without-output-specification
  (testing "Should be the IR stringified"
    (is (= ((:stitch-fn test-env)
            ["See here"
             {}
             ::ir/string
             {:url "#local"}
             ::ir/link
             {}
             ::ir/paragraph
             "A second paragraph."
             {}
             ::ir/string
             {}
             ::ir/paragraph
             {:title "Simple Doc"}
             ::ir/doc])
           (nut/weave test-env weave-doc|simple)))))

(deftest test-weave+
  (let [{:keys [env doc]} (nut/weave+ test-env weave-doc|simple)]
    (is (:ir-mapping env))
    (is (:stitch-fn env))
    (is (string? doc))))

(def weave-doc|childless
  [:glossa.weave/doc
   [:hr]])

(deftest test-parse|childless
  (is (= {:tag :glossa.weave/doc
          :attrs {}
          :content [{:tag :hr
                     :attrs {}
                     :content []}]}
         (nut/parse weave-doc|childless))))

(deftest test-compile-tree|childless
  (is (= [:glossa.weave.ir/no-content
          {}
          :glossa.weave.ir/horizontal-line
          {}
          :glossa.weave.ir/doc]
         (let [doc weave-doc|childless
               doc-tree (nut/parse doc)
               {:keys [ir]} (nut/emit test-env doc-tree)]
           ir))))

(def weave-doc|ol
  [:glossa.weave/doc
   [:ol
    [:li "alpha"]
    [:item "beta"]
    [:li "gamma"]]])

(deftest test-parse|ol
  (is (= {:tag :glossa.weave/doc,
          :attrs {},
          :content
          [{:tag :ol,
            :attrs {},
            :content
            [{:tag :li, :attrs {}, :content ["alpha"]}
             {:tag :item, :attrs {}, :content ["beta"]}
             {:tag :li, :attrs {}, :content ["gamma"]}]}]}
         (nut/parse weave-doc|ol))))

(deftest test-emit|ol
  (testing "Default IR for node type that in Markdown requires a unique api/emit method."
    (is (= ["alpha"
            {}
            :glossa.weave.ir/string
            {}
            :glossa.weave.ir/list-item
            "beta"
            {}
            :glossa.weave.ir/string
            {}
            :glossa.weave.ir/list-item
            "gamma"
            {}
            :glossa.weave.ir/string
            {}
            :glossa.weave.ir/list-item
            {}
            :glossa.weave.ir/ordered-list
            {}
            :glossa.weave.ir/doc]
           (->> weave-doc|ol
                (nut/parse)
                (nut/emit test-env)
                :ir)))))

(deftest test-stitch-default
  (let [stitch-fn (:stitch-fn test-env)]
    (is (= "This\nand\nThat\n"
           (stitch-fn ["This" "\n" "and" "\n" "That" " \n\n\t"])))))

(def weave-doc|raw
  [::nut/doc
   [:pre {:lang :clj}
    [:raw
     '(require '[glossa.weave :as weave])
     [1 2 3 4]]]])

(defmethod api/parse :raw
  [node]
  (api/parse-shallow node))

(deftest test-parse|raw
  (is (= {:tag ::nut/doc
          :attrs {}
          :content [{:tag :pre
                     :attrs {:lang :clj}
                     :content [{:tag :raw
                                :attrs {}
                                :content ['(require (quote [glossa.weave :as weave]))
                                          [1 2 3 4]]}]}]}
         (nut/parse weave-doc|raw))))

(def weave-doc|raw-oops
  [::nut/doc
   [:pre {:lang :clj}
    '(require '[glossa.weave :as weave])
    [1 2 3 4]]])

(deftest test-parse|raw-oops
  (is (= {:tag ::nut/doc
          :attrs {}
          :content
          [{:tag :pre
            :attrs {:lang :clj}
            :content
            [{:tag 'require
              :attrs {}
              :content
              [{:tag 'quote
                :attrs {}
                :content
                [{:tag 'glossa.weave,
                  :attrs {}
                  :content [:as 'weave]}]}]}
             {:tag 1
              :attrs {}
              :content [2 3 4]}]}]}
         (nut/parse weave-doc|raw-oops))))

(def weave-doc|unhandled-tag
  [::nut/doc
   [:oops]])

(deftest test-emit|unhandled-tag
  (is (= "**WEAVE DEBUG** ATTRS: #:glossa.weave.ir{:unhandled-tag :oops} // CONTENT: :glossa.weave.ir/no-content\n\n{}:glossa.weave.ir/doc\n"
         (nut/weave test-env weave-doc|unhandled-tag))))
