(ns user
  (:require
    [clojure.repl :refer [apropos doc source]]
    [glossa.weave.api :as api]
    [sc.api]))

(comment

  (sc.api/dispose-all!)

  ;; Portal
  (require '[portal.api :as p])
  (p/open) ; Open a new inspector
  (add-tap #'p/submit) ; Add portal as a tap> target
  (tap> :hello) ; Start tapping out values
  (p/clear) ; Clear all values
  (tap> :world) ; Tap out more values
  (remove-tap #'p/submit) ; Remove portal from tap> targetset
  (p/close) ; Close the inspector when done

  )
