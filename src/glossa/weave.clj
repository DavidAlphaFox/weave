(ns glossa.weave
  (:require
   [clojure.string :as str]
   [glossa.weave.api :as api]))

;;
;; Parsing to Tree of Maps
;;
;; Transform a raw Weave document into a consistent, map-based tree.
;;

;; TODO Consider empty arity/empty node and what it might return.
;; TODO Since it appears that metadata applied via tagged literal is not preserved, consider whether a more primitive method for identifying already-parsed nodes is desirable, e.g., explicit support in this function for something like [::weave/raw x].
(defn parse
  {:arglists '([weave-doc])}
  [node]
  (api/parse node))

;;
;; Compilation to IR
;;
;; Transform a parsed document tree into a sequence of commands (as data)
;; that can be trivially interpreted by the renderer.
;;
;; Document references are resolved.
;;

(defn emit
  [env parsed-doc]
  (api/emit env [] parsed-doc))

;;
;; Rendering to Strings
;;
;; Interpret the IR produced by `compile` to produce strings in specific
;; output formats. To add support for a new output format, you need only
;; implement the handful of multimethods responsible for this interpretation.
;;

;; TODO Benchmark processing IR instructions in parallel. Could factor at boundaries of defined words.
;; TODO Consider story for whether to escape strings based on output format.
(defn render
  [env ir-instructions]
  (reduce
   (fn [{:keys [env stack]} instruction]
     (api/render env stack instruction))
   {:env   env
    :stack []}
   ir-instructions))

;;
;; Stitching Final Document
;;

(defn stitch
  [{:keys [stitch-fn] :as _env} stack]
  (stitch-fn stack))

;;
;; Weaving - Putting it all together
;;

(def default-env api/default-env)
(def env api/env)
(alter-meta! #'env merge (dissoc (meta #'api/env) [:line :column :file :ns]))

(defn weave
  "Produce a string of your rendered document given a `weave-doc` tree."
  ([weave-doc] (weave (env) weave-doc))
  ([env weave-doc]
   (let [doc-tree (parse weave-doc)
         {:keys [env ir]} (emit env doc-tree)
         {:keys [env stack]} (render env ir)
         stitched (stitch env stack)]
     stitched)))

(defn weave+
  "Return a map of:

   - `:doc` The final document, as a string
   - `:env` The final environment used during document processing

  Custom implementations of `api/emit` or `api/render` can tuck values
  into the `env` argument they receive. This function exposes those values
  for use after document processing.
  "
  ([weave-doc] (weave (env) weave-doc))
  ([env weave-doc]
   (let [doc-tree (parse weave-doc)
         {:keys [env ir]} (emit env doc-tree)
         {:keys [env stack]} (render env ir)
         stitched (stitch env stack)]
     {:env env
      :doc stitched})))

(def default-line-comment ";; ")
(def default-display-width 80)

(defn- narrow-line
  [line opts]
  (let [{:keys [display-width line-comment]
         :or {display-width default-display-width
              line-comment default-line-comment}} opts
        prefix-width (count line-comment)
        target (- display-width prefix-width)
        words (str/split line #" ")
        {:keys [lines line]} (reduce
                              (fn [{:keys [line line-count lines]} word]
                                (let [word-count (count word)]
                                  (cond
                                    (> (+ line-count word-count) target)
                                    {:lines (conj lines (pop #_the-last-space line))
                                     :line [word " "]
                                     :line-count (inc word-count)}

                                    :else
                                    {:lines lines
                                     :line (conj line word " ")
                                     :line-count (+ line-count word-count 1)})))
                              {:lines []
                               :line-count 0
                               :line []}
                              words)]
    (->> (conj lines (pop #_the-last-space line))
         (map (partial str/join ""))
         (map (partial str line-comment))
         (str/join "\n"))))

(defn format-for-repl
  ([string] (format-for-repl string {}))
  ([string opts]
   (let [{:keys [line-comment]
          :or {line-comment default-line-comment}} opts
         lines (str/split-lines string)
         in-clj-block? (volatile! false)]
     (reduce
      (fn [s line]
        (cond
           ;; NOTE: Nested ``` explicitly unsupported and will break.
          (and @in-clj-block? (= line "```"))
          (do
            (vreset! in-clj-block? false)
            (str s line-comment line "\n"))

          @in-clj-block?
          (str s line "\n")

          (= line "```clj")
          (do
            (vreset! in-clj-block? true)
            (str s line-comment line "\n"))

          :else
          (str s (narrow-line line opts) "\n")))
      ""
      lines))))

(defn readme
  ([] (readme {:version (System/getenv "WEAVE_VERSION") :sha (System/getenv "WEAVE_SHA") :tag (System/getenv "WEAVE_TAG")}))
  ([{:keys [version sha tag]}]
   (when (or (str/blank? version)
             (str/blank? sha)
             (str/blank? tag))
     (throw (ex-info "Environment vars missing."
                     {:error :missing-env-vars
                      :WEAVE_VERSION version
                      :WEAVE_SHA sha
                      :WEAVE_TAG tag})))
   [::doc
    [:h1 "Weave"]

    [:p "The Glossa Weave library defines an API for transforming documents written as trees of Clojure data into human-readable strings."]
    (when-not (= version "skip")
      [:pre {:lang "clj"}
       (str
        ";; Git dep\n"
        (format "{dev.glossa/weave\n {:git/url \"https://gitlab.com/glossa/weave.git\"\n  :git/tag \"%s\"\n  :git/sha \"%s\"}}"
                tag sha)
        "\n\n;; Maven dep at https://clojars.org/dev.glossa/weave\n"
        (format "{dev.glossa/weave {:mvn/version \"%s\"}}"
                version))])

    [:p [:strong "No output format"] " is provided as part of this library. Add "
     [:a {:href "https://gitlab.com/glossa/weave-markdown"} "Weave Markdown"]
     " to your classpath to get simple Markdown output for " [:code "(glossa.weave/weave your-doc)"]]

    [:h2 "Development"]
    [:p [:em "NB: Review the scripts and `deps.edn` files for necessary aliases (not all of which are defined in this repo)."]]
    [:p "Please run the setup script to populate Git hooks:"]
    [:pre {:lang "shell"} "./script/setup"]
    [:p "To start nREPL (bring your own `:cider` alias):"]
    [:pre {:lang "shell"} "./script/dev"]
    [:p "To run tests:"]
    [:pre {:lang "shell"} "./script/test"]
    [:p "To run everything that CI does:"]
    [:pre {:lang "shell"} "./script/ci"]

    [:h2 "License"]
    [:p "Copyright © Daniel Gregoire, 2021"]
    [:p "THE ACCOMPANYING PROGRAM IS PROVIDED UNDER THE TERMS OF THIS ECLIPSE PUBLIC LICENSE (\"AGREEMENT\"). ANY USE, REPRODUCTION OR DISTRIBUTION OF THE PROGRAM CONSTITUTES RECIPIENT'S ACCEPTANCE OF THIS AGREEMENT."]]))

(comment
  (println (format-for-repl (weave (readme (zipmap [:version :sha :tag] (repeat "skip")))))))
